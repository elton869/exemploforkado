<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Employees extends Migration
{
    public function up()
    {
      Schema::create('employees', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->string('email')->unique();
          $table->timestamps();
      });
    }
    
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
