<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDepartmentIdToEmployees extends Migration
{
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->integer('department_id')->after('email');
            $table->foreign('department_id')->references('id')->on('departments');
        });
    }

    public function down()
    {
        $table->dropForeign(['department_id']);
    }
}