<?php

use Illuminate\Database\Seeder;

class DepartmentTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('departments')->insert([
            'name' => 'Administração',
        ]);
        DB::table('departments')->insert([
            'name' => 'RH',
        ]);
        DB::table('departments')->insert([
            'name' => 'Financeiro',
        ]);
    }
}
