@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Editar Funcionário</div>
                  <div class="panel-body">
                    @if($errors->any())
                      <div class="alert alert-danger">
                       @foreach ($errors->all() as $error)
                         * {{ $error }}<br>
                       @endforeach
                      </div>
                    @endif
                    <form action="{{ route('employees.update', $employee->id) }}" method="POST">
                      {!! csrf_field() !!}
                      <input type="hidden" name="_method" value="PUT">
                      <div class="form-group">
                        <label for="InputName">Nome</label>
                        <input class="form-control" type="text" name="name" value="{{ $employee->name }}">
                      </div>
                      <div class="form-group">
                        <label for="InputEmail">Email</label>
                        <input class="form-control" type="text" name="email" value="{{ $employee->email }}">
                      </div>
                      <label class="control-label">Departamento</label>
                        <div class="selectContainer" style="margin-bottom: 20px;">
                            <select class="form-control" name="department_id">
                                <option value="">Escolha um departamento</option>
                                @foreach ($departments as $department)
                                  <option value="{{ $department->id }}">{{ $department->name }}</option>
                                @endforeach
                            </select>
                        </div>
                      <button type="submit" class="btn btn-success btn-block">Editar</button>
                      <a href="{{ route('employees.index') }}" class="btn btn-primary" style="margin-top: 10px;"><< Voltar</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
