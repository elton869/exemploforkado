@extends('layouts.app')

@section('styles')
  <link href="{{ asset('css/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Informações do Funcionário</div>
                  <div class="panel-body">
                    PDF: <a href="{{ route('pdf.single', $employee->id) }}" target="_blank" title="PDF"><i class="fa fa-file-pdf-o" aria-hidden="true" style="font-size: 22px;"></i></a><br>
                    Excel: <a href="{{ route('excel.single', $employee->id) }}" title="Excel"><i class="fa fa-table" aria-hidden="true" style="font-size: 22px; color: green;"></i></a>
                    <table class="table table-striped" style="margin-top: 20px;">
                      <tbody>
                          <tr>
                            <th>ID:</th>
                            <td>{{ $employee->id }}</td>
                          </tr>
                          <tr>
                            <th>Nome:</th>
                            <td>{{ $employee->name }}</td>
                          </tr>
                          <tr>
                            <th>Email:</th>
                            <td>{{ $employee->email }}</td>
                          </tr>
                          <tr>
                            <th>Departamento:</th>
                            <td>{{ $employee->department['name'] }}</td>
                          </tr>
                      </tbody>
                    </table>
                    <a href="{{ route('employees.index') }}" class="btn btn-primary"><< Voltar</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
