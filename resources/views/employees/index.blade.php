@extends('layouts.app')

@section('styles')
  <link href="{{ asset('css/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Gerenciamento de Funcionários</div>
                  <div class="panel-body">
                    @if (Session::has('success'))
                      <div class="alert alert-success">{{ Session::get('success') }}</div>
                    @elseif (Session::has('failed'))
                      <div class="alert alert-danger">{{ Session::get('failed') }}</div>
                    @endif
                    <a href="{{ route('employees.create') }}" class="btn btn-success" style="float: right; margin-bottom: 15px;">Novo Funcionário</a>
                    <hr style="clear: both;">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Nome</th>
                          <th>Email</th>
                          <th>Departamento</th>
                          <th>Ação</th>
                          <th>#</th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach ($employees as $employee)
                            <tr>
                              <th scope="row">{{ $employee->id }}</th>
                              <td>{{ $employee->name }}</td>
                              <td>{{ $employee->email }}</td>
                              <td>{{ $employee->department['name'] }}</td>
                              <td>
                                <a href="{{ route('employees.show', $employee->id) }}" title="Visualizar"><i class="fa fa-eye" aria-hidden="true" style="font-size: 22px; color: blue;"></i></a>
                                <a href="{{ route('employees.edit', $employee->id) }}" title="Editar"><i class="fa fa-pencil" aria-hidden="true" style="font-size: 22px; color: black; margin-left: 5px;"></i></a>
                                <form style="display: inline-block;" action="{{ route('employees.destroy', $employee->id) }}" method="POST">
                                   {{ csrf_field() }}
                                   <input type="hidden" name="_method" value="DELETE">
                                   <button type="submit" style="background: none; border: none;"><i title="Excluir" class="fa fa-trash" aria-hidden="true" style="font-size: 22px;"></i></a>
                                </form>
                              </td>
                              <td>
                                <a href="{{ route('pdf.single', $employee->id) }}" target="_blank" title="PDF"><i class="fa fa-file-pdf-o" aria-hidden="true" style="font-size: 22px;"></i></a>
                                <a href="{{ route('excel.single', $employee->id) }}" title="Excel"><i class="fa fa-table" aria-hidden="true" style="margin-left: 5px; font-size: 22px; color: green;"></i></a>
                              </td>
                            </tr>
                          @endforeach
                      </tbody>
                    </table>
                    <div style="float: right;">
                      Lista PDF: <a href="{{ route('pdf.list') }}" target="_blank" title="PDF"><i class="fa fa-file-pdf-o" aria-hidden="true" style="font-size: 20px;"></i></a><br>
                      Lista Excel: <a href="{{ route('excel.list') }}" title="Excel"><i class="fa fa-table" aria-hidden="true" style="font-size: 20px; color: green;"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
