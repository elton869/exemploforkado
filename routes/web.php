<?php
Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::resource('/employees', 'EmployeesController')->middleware('auth');

Route::prefix('pdf')->group(function () {
	Route::get('/list', 'EmployeesController@pdflist')->name('pdf.list');
    Route::get('/{id}', 'EmployeesController@pdfsingle')->name('pdf.single');
});

Route::prefix('excel')->group(function () {
	Route::get('/list', 'EmployeesController@excellist')->name('excel.list');
    Route::get('/{id}', 'EmployeesController@excelsingle')->name('excel.single');
});
