<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EmployeeRequest;
use App\Employee;
use App\Department;
use Session;
use PDF;
use Excel;

class EmployeesController extends Controller
{
    public function index()
    {
        $employees = Employee::all()->sortByDesc('id');
        return view('employees.index')->withEmployees($employees);
    }

    public function create(Department $department)
    {
        return view('employees.create')->withDepartments($department->all());
    }

    public function store(EmployeeRequest $request)
    {
        if (Employee::create($request->all())) {
          return redirect()->route('employees.index')->with(Session::flash('success', 'O funcionário foi cadastrado com sucesso!'));
        }

        return redirect()->route('employees.index')->with(Session::flash('failed', 'Ocorreu algum erro ao tentar cadastrar o funcionário, tente novamente.'));
    }

    public function show(Employee $employee)
    {
        return view('employees.show')->withEmployee($employee);
    }

    public function edit(Employee $employee, Department $department)
    {
        return view('employees.edit')->with(['employee' => $employee, 'departments' => $department->all()]);
    }

    public function update(EmployeeRequest $request, Employee $employee)
    {
        if (Employee::where('id', $employee->id)->update(request()->except('_token', '_method'))) {
          return redirect()->route('employees.index')->with(Session::flash('success', 'O funcionário foi editado com sucesso!'));
        }

        return redirect()->route('employees.index')->with(Session::flash('failed', 'Ocorreu algum erro ao tentar editar o funcionário, tente novamente.'));
    }

    public function destroy(Employee $employee)
    {
        if ($employee->delete()) {
          return redirect()->route('employees.index')->with(Session::flash('success', 'O funcionário foi deletado com sucesso!'));
        }

        return redirect()->route('employees.index')->with(Session::flash('failed', 'Ocorreu algum erro ao tentar deletar o funcionário.'));
    }

    public function pdflist() {
        $pdf = PDF::loadView('employees.pdf.pdflist', ['employees' => Employee::all()->sortByDesc('id')]);
        return $pdf->stream();
    }

    public function pdfsingle($id) {
        $pdf = PDF::loadView('employees.pdf.pdfsingle', ['employee' => Employee::find($id)]);
        return $pdf->stream();
    }

    public function excellist() {
        $data = Employee::all()->sortByDesc('id');
        Excel::create('Employees', function($excel) use($data) { 
            $excel->sheet('EmployeeSheet', function($sheet) use($data) {
                $sheet->mergeCells('A1:B1');
                $sheet->cell('A1', function($cell) {
                     $cell->setValue('Funcionários');
                     $cell->setAlignment('center');
                     $cell->setFontWeight('bold');
                     $cell->setFontSize(18);
                });
                for($i = 2; $i < (4 * Employee::count()); $i++) {
                    for ($a = 0; $a < Employee::count(); $a++) {
                        $sheet->cell('A'.$i, function($cell) {
                            $cell->setValue('ID:');
                            $cell->setFontWeight('bold');
                        });
                        $sheet->cell('A'.($i+1), function($cell) {
                            $cell->setValue('Nome:');
                            $cell->setFontWeight('bold');
                        });
                        $sheet->cell('A'.($i+2), function($cell) {
                            $cell->setValue('Email:');
                            $cell->setFontWeight('bold');
                        });
                        $sheet->cell('A'.($i+3), function($cell) {
                            $cell->setValue('Departamento:');
                            $cell->setFontWeight('bold');
                        });
                        $sheet->cell('A'.($i+4), function($cell) {
                            $cell->setBackground('vbBlack');
                        });
                        $sheet->cell('B'.($i), function($cell) use($data, $i, $a) {
                            $cell->setValue($data[$a]->id);
                            $cell->setAlignment('justify');
                        });
                        $sheet->cell('B'.($i+1), function($cell) use($data, $i, $a) {
                            $cell->setValue($data[$a]->name);
                        });
                        $sheet->cell('B'.($i+2), function($cell) use($data, $i, $a) {
                            $cell->setValue($data[$a]->email);
                        });
                        $sheet->cell('B'.($i+3), function($cell) use($data, $i, $a) {
                            $cell->setValue($data[$a]->department['name']);
                        });
                        $sheet->cell('B'.($i+4), function($cell) {
                            $cell->setBackground('vbBlack');
                        });

                        $sheet->mergeCells('A'.($i+4).':B'.($i+4));
                        $i = $i + 5;
                    }
                    $a = 0;
                }
            });
        })->export('xlsx');
    }

    public function excelsingle($id) {
        $data = Employee::find($id);
        Excel::create('Employee', function($excel) use($data) { 
            $excel->sheet('EmployeeSheet', function($sheet) use($data) {
                $sheet->mergeCells('A1:B1');
                $sheet->cell('A1', function($cell) {
                     $cell->setValue('Funcionário');
                     $cell->setAlignment('center');
                     $cell->setFontWeight('bold');
                     $cell->setFontSize(18);
                });
                $sheet->cell('A2', function($cell) {
                    $cell->setValue('ID:');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('A3', function($cell) {
                    $cell->setValue('Nome:');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('A4', function($cell) {
                    $cell->setValue('Email:');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('A5', function($cell) {
                    $cell->setValue('Departamento:');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('B2', function($cell) use($data) {
                    $cell->setValue($data->id);
                    $cell->setAlignment('justify');
                });
                $sheet->cell('B3', function($cell) use($data) {
                    $cell->setValue($data->name);
                });
                $sheet->cell('B4', function($cell) use($data) {
                    $cell->setValue($data->email);
                });
                $sheet->cell('B5', function($cell) use($data) {
                    $cell->setValue($data->department['name']);
                });
            });
        })->export('xlsx');
    }
}
